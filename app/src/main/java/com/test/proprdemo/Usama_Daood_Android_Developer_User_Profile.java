package com.test.proprdemo;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class Usama_Daood_Android_Developer_User_Profile extends AppCompatActivity implements GoogleMap.OnMarkerClickListener {


    @BindView(R.id.name_tv) TextView name_tv;
    @BindView(R.id.email_tv) TextView email_tv;
    @BindView(R.id.phone_tv) TextView phone_tv;
    @BindView(R.id.user_pic) ImageView user_pic;
    @BindView(R.id.mapview) MapView mMapView;
    String name, email, phone_num, user_id, user_lat, user_lng;
    private GoogleMap googleMap;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__profile);
        ButterKnife.bind(this);
        context = Usama_Daood_Android_Developer_User_Profile.this;

        try{
            // Back Button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }catch (Exception b){
            System.out.println("Log. Error " + b.toString());
        }

        if (getIntent().getExtras() != null) {
            name = getIntent().getStringExtra("user_name");
            email = getIntent().getStringExtra("user_email");
            phone_num = getIntent().getStringExtra("user_phone");
            user_id = getIntent().getStringExtra("user_id");
            user_lat = getIntent().getStringExtra("user_lat");
            user_lng = getIntent().getStringExtra("user_lng");

            // Displaying Values....
            name_tv.setText(name);
            email_tv.setText(email);
            phone_tv.setText(phone_num);

        }




        try {

            // Google Map
            mMapView.onCreate(savedInstanceState);

            // Call Back
            mMapView.onResume(); // needed to get the map to display immediately

            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                googleMap.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map
                LatLng sydney = new LatLng(Double.parseDouble(user_lat), Double.parseDouble(user_lng));

                googleMap.addMarker(new MarkerOptions().position(sydney).title("User Location").snippet("User Location"));

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


            }


        });


    }


    /**
     * Back Button Handling....
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        System.out.println("Log. Click");

        return true;
    }


//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        map = googleMap;
//        map.getUiSettings().setZoomControlsEnabled(true);
//        map.addMarker(new MarkerOptions().position(new LatLng(43.1, -87.9)));
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(43.1, -87.9), 10));
//    }



}
