package com.test.proprdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

public class Usama_Daood_Android_Developer_MainActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    Context context;

//    John_Smith_Android_Developer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        context = Usama_Daood_Android_Developer_MainActivity.this;

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */


                String user_inf = Usama_Daood_Android_Developer_SharedPrefrence.get_offline(context,Usama_Daood_Android_Developer_SharedPrefrence.shared_user_login_detail_key);
                if(user_inf != null){
                    // Open Home Page

                    Intent myIntent = new Intent(context, Usama_Daood_Android_Developer_Home.class);
//                myIntent.putExtra("user_id", user_id);
//                myIntent.putExtra("email", email);
//                myIntent.putExtra("user_name", user_name);
//                myIntent.putExtra("token", token);
//                myIntent.putExtra("pic_url", user_pic);
                    startActivity(myIntent);
                    finish();


                }else{
                    // Open Sign in page

                    Intent myIntent = new Intent(context , Usama_Daood_Android_Developer_Login.class);
                    startActivity(myIntent);
                    finish();

                }



            }
        }, SPLASH_DISPLAY_LENGTH);




    }
}
