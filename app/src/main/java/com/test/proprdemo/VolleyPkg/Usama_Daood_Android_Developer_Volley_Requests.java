package com.test.proprdemo.VolleyPkg;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.test.proprdemo.Utils.Usama_Daood_Android_Developer_Variables;

import org.json.JSONObject;

public class Usama_Daood_Android_Developer_Volley_Requests {

    // TODO: New mEthod

    public static void New_Volley(final Context context, final String API_link,
                                  final JSONObject Send_Data, final Usama_Daood_Android_Developer_CallBack usamaDaoodAndroidDeveloperCallBack){


        try {
            RequestQueue queue = Volley.newRequestQueue(context);
            JsonObjectRequest jsonObj = new JsonObjectRequest(API_link,Send_Data, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if(usamaDaoodAndroidDeveloperCallBack != null)
                        usamaDaoodAndroidDeveloperCallBack.Get_Response("Post",response.toString(), API_link);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(usamaDaoodAndroidDeveloperCallBack != null)
                        usamaDaoodAndroidDeveloperCallBack.Get_Response("Post",error.toString(), API_link);

                }
            });

            queue.add(jsonObj);
            jsonObj.setRetryPolicy(new DefaultRetryPolicy(
                    Usama_Daood_Android_Developer_Variables.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        }catch(Exception e){

        }

    }

}
