package com.test.proprdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.test.proprdemo.Adapters.Usama_Daood_Android_Developer_Users_adapter;
import com.test.proprdemo.DataModel.Usama_Daood_Android_Developer_allUsersModel;
import com.test.proprdemo.VolleyPkg.Usama_Daood_Android_Developer_API_LINKS;
import com.test.proprdemo.VolleyPkg.Usama_Daood_Android_Developer_CallBack;
import com.test.proprdemo.VolleyPkg.Usama_Daood_Android_Developer_Volley_Requests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Usama_Daood_Android_Developer_Home extends AppCompatActivity {

    @BindView(R.id.users_recycler) RecyclerView users_recycler;
    @BindView(R.id.search_users) EditText Edit_search_users;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    private List<Usama_Daood_Android_Developer_allUsersModel> People_Lists = new ArrayList<>() ;
    Context context;
    Usama_Daood_Android_Developer_Users_adapter adapter;

    String user_lat = "40.71427";
    String user_lng = "-74.00597";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usama__daood__android__developer__home);
        context = Usama_Daood_Android_Developer_Home.this;
        ButterKnife.bind(this);

        Edit_search_users.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int len = Edit_search_users.length();

                filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });


        progressBar.setVisibility(View.VISIBLE);

        get_all_users();

    }


    void filter(String text){

        List<Usama_Daood_Android_Developer_allUsersModel> temp = new ArrayList();
        Usama_Daood_Android_Developer_allUsersModel Wall;
        for(Usama_Daood_Android_Developer_allUsersModel d: People_Lists){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if(d.getName().toLowerCase().contains(text.toLowerCase())){
                temp.add(d);
            }
        }
        //update recyclerview
        adapter.updateList((ArrayList<Usama_Daood_Android_Developer_allUsersModel>) temp);

    }


    public void get_all_users (){

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("", "");
            //parameters.put("password", password);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Usama_Daood_Android_Developer_Volley_Requests.New_Volley(
                context,
                Usama_Daood_Android_Developer_API_LINKS.API_Get_People,
                parameters,
                new Usama_Daood_Android_Developer_CallBack() {
                    @Override
                    public void Get_Response(String requestType, String response, String url) {
                        //progress_circular.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        System.out.println("Resp: " + response);
                        //handleResponse(response);

                        handleResponse(response);


                    }
                }
        );




    } // End method to get All Users

    public void handleResponse (String resp) {
        try {

            JSONObject response = new JSONObject(resp);

            JSONArray people_arr = response.getJSONArray("People");

            for(int i=0;i< people_arr.length();i++){
                JSONObject obj = people_arr.getJSONObject(i);
                obj.getString("name");
                obj.getString("email");
                obj.getString("phone_no");
                obj.getString("user_id");
                obj.getString("gender");


                Usama_Daood_Android_Developer_allUsersModel people = new Usama_Daood_Android_Developer_allUsersModel(
                        "" + obj.getString("name"),
                        "" + obj.getString("user_id"),
                        "",
                        "" + obj.getString("email"),
                        "" + obj.getString("phone_no"),
                        "" + user_lat,
                        "" + user_lng
                );

                People_Lists.add(people);
            }  // ENd for Loop

            // Setting up Adapters

            adapter = new Usama_Daood_Android_Developer_Users_adapter(People_Lists, context);
            users_recycler.setLayoutManager(new LinearLayoutManager(context));
            users_recycler.setHasFixedSize(false);
            users_recycler.setAdapter(adapter);




        }catch (Exception b){
            System.out.println("Log. Error " + b.toString());
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.logout) {

// Display Alert
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Exit App")
                    .setMessage("Are you sure you want to Logout?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Usama_Daood_Android_Developer_SharedPrefrence.logout_user(context);

                        }

                    })
                    .setNegativeButton("No", null)
                    .show();



        }
        return super.onOptionsItemSelected(item);
    }






}
