package com.test.proprdemo.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

public class Usama_Daood_Android_Developer_Methods {

    public static void toast_msg(Context cOntext, String msg){
        Toast.makeText(cOntext, ""+msg, Toast.LENGTH_SHORT).show();
        Log.d("" + cOntext,"Log: \n" + msg);
    }


    /// Alert Dialogue
    public static void alert_dialogue(final Context context, String title, String msg){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle(""+title);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();

                    }
                });

//        builder1.setNegativeButton(
//                "No",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                    }
//                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


}
