package com.test.proprdemo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class Usama_Daood_Android_Developer_SharedPrefrence {

    public static SharedPreferences.Editor editor;
    public static SharedPreferences pref;
    public static String shared_category_key = "category";
    public static String shared_saved_news_key = "saved";
    public static String shared_discover_news_key = "discover";
    public static String shared_user_login_detail_key = "user_info";
    public static String shared_home_food_key = "home_food";

    public static void init_share(Context context){
        pref = context.getSharedPreferences("ProprDemo", 0); // 0 - for private mode
        editor = pref.edit();
    }


    public static void save_response_share( Context context,String value,String data_key)
    {
        init_share(context);
        editor.putString(data_key, value); // Storing string
        editor.commit();
    }


    public static String get_offline(Context context, String key){
        init_share(context);
        pref.getString(key, null);
        // Usama_Daood_Android_Developer_Variables.toast_msg(context,"Value "+pref.getString(key, null));
        return pref.getString(key, null);

    }

    public static void logout_user(Context context)
    {
        init_share(context);
        boolean ch = pref.edit().remove(shared_user_login_detail_key).commit();
        boolean ch_1 = pref.edit().remove(shared_user_login_detail_key).commit();


        Intent intent = new Intent(context, Usama_Daood_Android_Developer_MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

}
