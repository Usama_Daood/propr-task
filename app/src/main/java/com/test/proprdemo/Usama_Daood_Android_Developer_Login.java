package com.test.proprdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.test.proprdemo.Utils.Usama_Daood_Android_Developer_Methods;
import com.test.proprdemo.VolleyPkg.Usama_Daood_Android_Developer_API_LINKS;
import com.test.proprdemo.VolleyPkg.Usama_Daood_Android_Developer_CallBack;
import com.test.proprdemo.VolleyPkg.Usama_Daood_Android_Developer_Volley_Requests;

import org.json.JSONException;
import org.json.JSONObject;

public class Usama_Daood_Android_Developer_Login extends AppCompatActivity {
    // INit Views
    @BindView(R.id.email)
    EditText edit_email;
    @BindView(R.id.password)
    EditText edit_password;
    @BindView(R.id.buttonSignin)
    Button buttonSignin;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    Context context;

    public void Runtime_permission (){

        ActivityCompat.requestPermissions(Usama_Daood_Android_Developer_Login.this,
                new String[]{
                        android.Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION

                },
                1);

    } // End method tofor Runtime Permission.


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usama__daood__login);
        context = Usama_Daood_Android_Developer_Login.this;
        ButterKnife.bind(this);

        // todo: Add Run time Permission.
        Runtime_permission();

    }

    // On CLick Listener by Using ButterKnife
    @OnClick({R.id.buttonSignin, R.id.Register})
    public void Click(View view){
        if(view.getId() == R.id.buttonSignin){
            // Login User

            getLogin();
        }

        if(view.getId() == R.id.Register){
            // Open Register Screen
            openRegister();
        }




    } // End On CLick Listener Using Butter Knifes


    public void getLogin() {

        String email = edit_email.getText().toString();
        String password = edit_password.getText().toString();


        if (TextUtils.isEmpty(email)) {
            edit_email.setError(getResources().getString(R.string.for_empty_email));
            edit_email.requestFocus();
            return;
        } else if (TextUtils.isEmpty(password)) {
            edit_password.setError(getResources().getString(R.string.for_empty_password));
            edit_password.requestFocus();
            return;
        } else {
            // Calling API

            progressBar.setVisibility(View.VISIBLE);
            SimpleLogin(email, password);


        }  // End else Block


    }


    public void SimpleLogin (String email, String password) {

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("email", email);
            parameters.put("password", password);

            // Calling API

            Usama_Daood_Android_Developer_Volley_Requests.New_Volley(
                    context,
                    Usama_Daood_Android_Developer_API_LINKS.API_SIGN_IN,
                    parameters,
                    new Usama_Daood_Android_Developer_CallBack() {
                        @Override
                        public void Get_Response(String requestType, String response, String url) {
                            //progress_circular.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            System.out.println("Resp: " + response);
                            //handleResponse(response);

                            handleResponse(response);


                        }
                    }
            );



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }  // End login


    public void handleResponse (String resp){
        try{

            JSONObject response = new JSONObject(resp);


            String code = response.getString("code");
            if(code.equals("200")){

                // If login Successfull
                Intent myIntent = new Intent(context, Usama_Daood_Android_Developer_Home.class);
                startActivity(myIntent);
                finish();

                // Save data info into Shared Prefrence

                Usama_Daood_Android_Developer_SharedPrefrence.save_response_share(context,
                        "" + resp,
                        "" + Usama_Daood_Android_Developer_SharedPrefrence.shared_user_login_detail_key
                );


            }else{
                // If login failed

                Usama_Daood_Android_Developer_Methods.alert_dialogue(context,"Info","" + response.getString("msg"));


            }




        }catch (Exception b){
            System.out.println("Log. Error " + b.toString());

        } // End Catch Body


    }

    // Open Register Screen
    public void openRegister(){

        Intent myIntent = new Intent(context, Usama_Daood_Android_Developer_SignUp.class);
        startActivity(myIntent);
    }


}