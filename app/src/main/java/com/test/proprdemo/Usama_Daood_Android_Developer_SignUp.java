package com.test.proprdemo;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.test.proprdemo.Utils.Usama_Daood_Android_Developer_Methods;
import com.test.proprdemo.VolleyPkg.Usama_Daood_Android_Developer_API_LINKS;
import com.test.proprdemo.VolleyPkg.Usama_Daood_Android_Developer_CallBack;
import com.test.proprdemo.VolleyPkg.Usama_Daood_Android_Developer_Volley_Requests;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Usama_Daood_Android_Developer_SignUp extends AppCompatActivity {

    // INit View Using ButterKnife
    @BindView(R.id.name) EditText edit_name;
    @BindView(R.id.email) EditText edit_email;
    @BindView(R.id.mobile_num) EditText edit_mobile;
    @BindView(R.id.password) EditText edit_password;
    @BindView(R.id.gender) Spinner gender_spinner;
    @BindView(R.id.buttonRegister) Button buttonRegister;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    // Usama_Daood_Android_Developer_Variables
    ArrayList<String> gender_list = new ArrayList<String>();
    ArrayAdapter adapter;
    Context context;
    String gender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usama__daood__android__developer__sign_up);
        ButterKnife.bind(this);
        context = Usama_Daood_Android_Developer_SignUp.this;


        // Add date into Spinner
        gender_list.add("Male");
        gender_list.add("Female");
        adapter = new ArrayAdapter(context,R.layout.spinner_layout ,gender_list);
        gender_spinner.setAdapter(adapter);
        gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
                gender = gender_list.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

    }

    @OnClick({R.id.buttonRegister})
    public void onCLick(View view){
        if(view.getId() == R.id.buttonRegister){


            getUserData();
        }


    }

    // Get User Data for SignUp
    public void getUserData(){

        final String name = edit_name.getText().toString().trim();
        final String email = edit_email.getText().toString().trim();
        final String password = edit_mobile.getText().toString().trim();
        final String mobile_num = edit_password.getText().toString().trim();

        if (TextUtils.isEmpty(name)) {
            edit_name.setError(getResources().getString(R.string.for_empty_name));
            edit_name.requestFocus();
            return;
        } else
        if (TextUtils.isEmpty(email)) {
            edit_email.setError(getResources().getString(R.string.for_empty_email));
            edit_email.requestFocus();
            return;
        } else if (TextUtils.isEmpty(password)) {
            edit_password.setError(getResources().getString(R.string.for_empty_password));
            edit_password.requestFocus();
            return;
        }else if (TextUtils.isEmpty(mobile_num)) {
            edit_mobile.setError(getResources().getString(R.string.for_empty_phone));
            edit_mobile.requestFocus();
            return;
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            edit_email.setError(getResources().getString(R.string.for_valid_email));
            edit_email.requestFocus();
            return;
        }else{

            progressBar.setVisibility(View.VISIBLE);
            SimpleSignUp(
                    "" + name,
                    "" + mobile_num,
                    "" + email,
                    "" + password ,
                    "" + gender
            );


        }




        } // End Method to SignUp

    // SIgnup Method

    public void SimpleSignUp (String name, String mobile_num,String email, String password, String gender){

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("name", name);
            parameters.put("email", email);
            parameters.put("mobile_num", mobile_num);
            parameters.put("password", password);
            parameters.put("gender", gender);

            // Calling API
            Usama_Daood_Android_Developer_Volley_Requests.New_Volley(
                    context,
                    Usama_Daood_Android_Developer_API_LINKS.API_SIGN_UP,
                    parameters,
                    new Usama_Daood_Android_Developer_CallBack() {
                        @Override
                        public void Get_Response(String requestType, String response, String url) {
                            //progress_circular.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);

                            System.out.println("Resp: " + response);
                            //handleResponse(response);

                            handleResponse(response);


                        }
                    }
            );



        } catch (JSONException e) {
            e.printStackTrace();
        }
    } // Simple SignUp

    // Handling Response

    public void handleResponse (String resp) {
        try {
            JSONObject response = new JSONObject(resp);
            String code = response.getString("code");
            if(code.equals("200")) {
                // If Create Successfull
                Intent myIntent = new Intent(context, Usama_Daood_Android_Developer_Home.class);
                startActivity(myIntent);
                finish();

                // Save data info into Shared Prefrence

                Usama_Daood_Android_Developer_SharedPrefrence.save_response_share(context,
                        "" + resp,
                        "" + Usama_Daood_Android_Developer_SharedPrefrence.shared_user_login_detail_key
                );
            }else{

                Usama_Daood_Android_Developer_Methods.alert_dialogue(context,"info", "" + response.getString("msg"));

            }



            } catch (Exception b) {

        }



    } // End Method



}
