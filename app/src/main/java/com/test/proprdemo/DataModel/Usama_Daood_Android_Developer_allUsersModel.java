package com.test.proprdemo.DataModel;

public class Usama_Daood_Android_Developer_allUsersModel {
    String name;
    String user_id;
    String img_url;
    String email;
    String phone_num;
    String user_lat;
    String user_lng;


    public Usama_Daood_Android_Developer_allUsersModel(String name, String user_id, String img_url, String email, String phone_num, String user_lat, String user_lng) {
        this.name = name;
        this.user_id = user_id;
        this.img_url = img_url;
        this.phone_num = phone_num;
        this.email = email;
        this.user_lng = user_lng;
        this.user_lat = user_lat;

    }

    public String getUser_lat() {
        return user_lat;
    }

    public void setUser_lat(String user_lat) {
        this.user_lat = user_lat;
    }

    public String getUser_lng() {
        return user_lng;
    }

    public void setUser_lng(String user_lng) {
        this.user_lng = user_lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(String phone_num) {
        this.phone_num = phone_num;
    }
}
