package com.test.proprdemo.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.proprdemo.DataModel.Usama_Daood_Android_Developer_allUsersModel;
import com.test.proprdemo.R;
import com.test.proprdemo.Usama_Daood_Android_Developer_User_Profile;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class Usama_Daood_Android_Developer_Users_adapter extends RecyclerView.Adapter<Usama_Daood_Android_Developer_Users_adapter.PeopleViewHolder> {

    Context context;
    //  Images_DataModel add;
    private List<Usama_Daood_Android_Developer_allUsersModel> people_list;

    public Usama_Daood_Android_Developer_Users_adapter(List<Usama_Daood_Android_Developer_allUsersModel> people_list, Context context){
        this.context=context;
        this.people_list = people_list;
    }

    @NonNull
    @Override
    public Usama_Daood_Android_Developer_Users_adapter.PeopleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
//        image_slider_Model
        View view = inflater.inflate(R.layout.users_custom_layout,viewGroup,false);

        return new Usama_Daood_Android_Developer_Users_adapter.PeopleViewHolder(view);
    }
    public void updateList(ArrayList<Usama_Daood_Android_Developer_allUsersModel> list){
        people_list = list;
        notifyDataSetChanged();
    }

    public void onBindViewHolder(@NonNull final PeopleViewHolder peopleViewHolder, final int i) {
        Usama_Daood_Android_Developer_allUsersModel user_info = people_list.get(i);

        peopleViewHolder.user_name.setText(user_info.getName());


        Picasso.get()
                .load("http://baithek.com/" + user_info.getImg_url()).fit()
                .centerCrop()
                .placeholder(R.drawable.ic)
                .error(R.drawable.ic)
                .into(peopleViewHolder.people_pic);

        peopleViewHolder.header_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Open User Profile

                Intent myIntent = new Intent(context , Usama_Daood_Android_Developer_User_Profile.class);

                myIntent.putExtra("user_name", user_info.getName());
                myIntent.putExtra("user_email", user_info.getEmail());
                myIntent.putExtra("user_id", user_info.getUser_id());
                myIntent.putExtra("user_phone", user_info.getPhone_num());
                myIntent.putExtra("user_lat", user_info.getUser_lat());
                myIntent.putExtra("user_lng", user_info.getUser_lng());


                context.startActivity(myIntent);


            }
        });



    }



    @Override
    public int getItemCount() {
        return people_list.size();
    }


    public class PeopleViewHolder extends RecyclerView.ViewHolder{

        ImageView people_pic;
        TextView user_name;
        CardView header_1;
        public PeopleViewHolder(@NonNull View itemView) {
            super(itemView);
            people_pic = itemView.findViewById(R.id.user_pic);
            user_name = itemView.findViewById(R.id.user_name);
            header_1 = itemView.findViewById(R.id.header_1);

        }

        public void updateList (ArrayList<Usama_Daood_Android_Developer_allUsersModel> list){
            people_list = list;
            notifyDataSetChanged();
        }

    }






}
